CONTENTS OF THIS FILE
---------------------
 * Requirements
 * Configuration
 * More Information

REQUIREMENTS
------------
 * The contentprotector module requires the mimedetect module to be able to
   correctly serve up content to the user.
 * The contentprotector module uses .htaccess files to protect content, and
   thus works only on Apache with mod_rewrite enabled, or equivalent.

CONFIGURATION
-------------
Grant the "administer content protector" role to your administrative user,
and "access protected content" to the roles which should be able to view
the protected directory.

Access the configuration page and set the directory to be protected,
  /admin/content/contentprotector

MORE INFORMATION
----------------
See the project page at:
  http://drupal.org/project/contentprotector

